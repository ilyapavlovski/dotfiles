#! /bin/bash

## java spring shell
function jss() {

    ## make sure that running from the root dir - should have a pom.xml there
    if [[ ! -f pom.xml ]]; then
        echo "File pom.xml missing. Try again from root directory."
        return
    fi

    ## build the jar - if fail to build, stop going.
    mvn clean install -DskipTests
    if [[ $? != 0 ]]; then
        echo "Failed to compile. Exiting."
        return
    fi

    ## extract the jar to prep for classpath export
    cd target
    jar xf *.jar
    cd ..

    ## export the classpath
    export CLASSPATH="target/BOOT-INF/classes/:target/BOOT-INF/lib/*"

    ## run java shell with boot.jsh file, if it is there
    local args="-R -Dspring.profiles.active=test"
    local boot="src/main/resources/boot.jsh"
    if [[ -f "$boot" ]]; then
        jshell $args $boot
    else
        jshell $args
    fi
}

## use the 'config' wrapper to add changes, commit them and push them
function config-push() {
    config add -u
    config commit -m "update."
    config push
}

## calculator
function c() {
    printf "%s\n" "$*" | bc 
}


# Usage: grepc "query" -H
function grepc() {
   grep -P -hr -C 0 --group-separator=$'\n\n' "$@" ~/.scratches 
}

# Usage: grepl "query" -H
function grepl() {
   grep -P -hr -C 0 --group-separator=$'\n\n' "$@" ~/.tasknotes ~/documents/journal-archive
}


function grepo() {
    # get the command results
    local results="$(pcregrep -r -cl $1 $scratches | awk -F: '{ print $2 " " $1 }' | sort -hr)"

    if [[ $2 == "-o" ]]; then 
        echo -n $1 | xsel -bi
        echo "$results" | cut -d" " -f2 | xargs subl3 -n
    else
        echo "$results"
    fi
}


function find-grep() {
    ## $1 - extension
    ## $2 - query
    if [[ -z $1 ]]; then echo "Usage: find-grep 'ext' 'query'"; return; fi
    find . -type f -iname "*.$1" -exec grep --color=auto -iH "$2" {} \;
}




# Usage: grepc ...args
# Depends on `$blog` and `$scratches` to be there.
function clfu() {
    local clfu="$HOME/.clfu"
   # error check: variables blog and scratches. exit with message
   grep --color=always -P "$1" -B 1 --group-separator "" "$clfu" | less -FXR
}

## TODOs:
## -l     : list
## -x num : execute number
## -w num : go to the browser
function gitlab-snippets() {

    curl -s -H @$HOME/.secrets/gitlab-token "https://gitlab.com/api/v4/snippets" \
     | jq '.[] | [.id, .title] | @tsv' |  xargs printf "%b\n"

}

function gitlab-npx() {
    if [[ -z $1 ]]; then
        echo "No ID provided. Exiting."
    else
        npx git+https://git@gitlab.com:snippets/$1.git
    fi
}